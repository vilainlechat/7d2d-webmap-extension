class PoiList extends HTMLElement {
  constructor() {
    super();

    let shadow = this.attachShadow({mode: 'open'})
    let wrapper = document.createElement('div')

    let bulma = document.createElement('link');
    bulma.setAttribute('rel', 'stylesheet');
    bulma.setAttribute('href', browser.extension.getURL("css/bulma.min.css"));

    let style = document.createElement('style')
    style.textContent = `
      ::-moz-placeholder {
        color: #292929 !important;
      }

      .modal-content {
        background-color: white;
      }

      .poi > * {
        pointer-events: none;
      }

      .poi {
        cursor: pointer !important;
        margin: 5px;
      }

      .poi {
        border: 1px solid transparent;
      }

      .poi:hover {
        border: 1px solid #d4d4d4
      }

      .modal-content, .modal-card-head, .poi {
        font-family: BlinkMacSystemFont,-apple-system,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
      }
    `

    let modal = document.createElement('div')
    modal.classList = "modal"
    modal.id = "poiList"
    modal.style = "z-index: 1001 !important; text-align: center"

    modal.innerHTML = `
      <div class="modal-background closeModal"></div>
      <div class="modal-card" style="width: 80%">
        <header class="modal-card-head">
          <p class="modal-card-title">POIs list</p>
          <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body" id="modalContent">
          <div class="columns is-multiline">
          </div>
        </section>
        <footer class="modal-card-foot">
        </footer>
      </div>
    `

    let columnsDiv = modal.childNodes[3].childNodes[3].childNodes[1]

    const background = modal.childNodes[1]
    background.addEventListener('click', (e) => {
      modal.classList.remove("is-active")
    })

    const headClose = modal.childNodes[3].childNodes[1].childNodes[3]
    headClose.addEventListener('click', (e) => {
      modal.classList.remove("is-active")
    })

    let poisWithImage = []

    fetch(`http://${window.location.hostname}:${parseInt(window.location.port)+1}/api/getallpois?nofilter`)
    .then(function(response) {
      return response.text();
    })
    .then(function(data) {
      let json = JSON.parse(data)
      let splittedList = []
      json.AllPOIs.forEach((poi) => {
        splittedList.push(poi.name.split(".")[0])
      })


      let uniqList = splittedList.reduce(function(a,b){if(a.indexOf(b)<0)a.push(b);return a;},[]);
      uniqList.sort()

      uniqList.forEach((poi) => {
        let image = browser.runtime.getURL(`ressources/prefabs/${poi}.jpg`)

        let col = document.createElement('div')
        col.classList = "column poi is-2"
        col.innerHTML = `
          <div style="margin-bottom: 10px;">
            ${poi}
          </div>
          <img src="${image}" />
        `

        col.dataset.name = poi


        col.addEventListener('click', (ev) => {
          const poiName = ev.target.dataset.name
          let searchRoot = document.querySelector('poi-search').shadowRoot
          let searchInput = searchRoot.childNodes[2].childNodes[0]

          searchInput.value = poiName

          searchInput.dispatchEvent(new Event('input'))

          modal.classList.remove("is-active")
        }, true)

        columnsDiv.appendChild(col)
      })
    })

    wrapper.appendChild(modal)
    shadow.appendChild(bulma);
    shadow.appendChild(style);

    shadow.appendChild(wrapper);
  }
}
customElements.define('poi-list', PoiList)
