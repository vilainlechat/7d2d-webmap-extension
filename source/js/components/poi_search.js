class PoiSearch extends HTMLElement {
  constructor() {
    super();

    let shadow = this.attachShadow({mode: 'open'})
    let wrapper = document.createElement('div')

    let bulma = document.createElement('link');
    bulma.setAttribute('rel', 'stylesheet');
    bulma.setAttribute('href', browser.extension.getURL("css/bulma.min.css"));

    let style = document.createElement('style')
    style.textContent = `
      ::-moz-placeholder {
        color: #292929 !important;
      }
    `
    let searchInput = document.createElement('input')
    searchInput.classList = "input is-small"
    searchInput.style = "margin-top: 10px; padding: 0px 0px 0px 10px; width: 90%;"
    searchInput.placeholder = "POI Search..."

    searchInput.addEventListener('input', (ev) => {
      let checkboxes = document.getElementsByClassName("leaflet-control-layers-selector")
      let allPoisCheckbox = checkboxes[4]
      
      if (allPoisCheckbox.checked) {
        allPoisCheckbox.click()
        allPoisCheckbox.click()
      } else {
        allPoisCheckbox.click()
      }

      browser.runtime.sendMessage({filterPoiName: ev.target.value})
    })


    let listButton = document.createElement('button')
    listButton.innerText = "Liste"
    listButton.id = "listButton"

    listButton.addEventListener('click', (ev) => {
      let modalRoot = document.querySelector('poi-list').shadowRoot
      let modal = modalRoot.childNodes[2].childNodes[0]
      modal.classList.add('is-active')

      ev.preventDefault()
      ev.stopPropagation()
    }, true)

    wrapper.appendChild(searchInput)
    wrapper.appendChild(listButton)

    shadow.appendChild(bulma);
    shadow.appendChild(style);

    shadow.appendChild(wrapper);
  }
}
customElements.define('poi-search', PoiSearch);
