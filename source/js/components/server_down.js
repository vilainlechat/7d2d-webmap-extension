class ServerDown extends HTMLElement {
  constructor() {
    super();

    let shadow = this.attachShadow({mode: 'open'})
    let wrapper = document.createElement('div')

    let bulma = document.createElement('link');
    bulma.setAttribute('rel', 'stylesheet');
    bulma.setAttribute('href', browser.extension.getURL("css/bulma.min.css"));

    let style = document.createElement('style')
    style.textContent = `
      ::-moz-placeholder {
        color: #292929 !important;
      }

      h3.title {
        color: inherit !important;
        font-family: BlinkMacSystemFont,-apple-system,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
      }
    `

    let modal = document.createElement('div')
    modal.classList = "modal"
    modal.id = "serverDown"
    modal.style = "z-index: 1001 !important; text-align: center"
    modal.innerHTML = `
      <div class="modal-background closeModal"></div>
      <div class="modal-content" style="width: 80%">
        <h3 class="title is-3">
          The server is offline, the page will auto-reload when back
        </h3>
      </div>
    `

    wrapper.appendChild(modal)

    shadow.appendChild(bulma);
    shadow.appendChild(style);

    shadow.appendChild(wrapper);
  }
}
customElements.define('server-down', ServerDown);
