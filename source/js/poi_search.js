const { sendMessageToContentScript } = require('./send_message')

require('./components/poi_search.js')
require('./components/poi_list.js')
// require('./components/server_down.js')

exports.poiSearchContentScript = () => {

  //
  // Inject search form
  //
  const serverStatsElem = document.getElementById('serverstats')
  let container = document.createElement('poi-search')
  let modal = document.createElement('poi-list')
  let serverDown = document.createElement('server-down')

  // There is a bug with jquery, wait a little to avoid it
  setTimeout(function () {
    serverStatsElem.appendChild(container)
    document.body.appendChild(modal)
    // document.body.appendChild(serverDown)
  }, 200);

  // Listen for poi success request then change some colors to better identify pois on map
  browser.runtime.onMessage.addListener((request, sender) => {
    if (typeof(request.server_down) !== 'undefined') {

      if (request.server_down) {
        let serverDownModalRoot = document.querySelector('server-down').shadowRoot

        serverDownModal = serverDownModalRoot.childNodes[2].childNodes[0]
        serverDownModal.classList.add('is-active')

      } else {
        document.location.reload()
      }
    }

    if (typeof(request.pois_fetched) !== 'undefined') {
      let interval = setInterval(function () {
        let poiSvgElements = document.querySelectorAll("[stroke='#EBF551']")

        if (poiSvgElements.length) {
          clearInterval(interval)

          for (var svgElement in poiSvgElements) {
            if (poiSvgElements.hasOwnProperty(svgElement)) {
              poiSvgElements[svgElement].setAttribute('fill', "#ff0000")
              poiSvgElements[svgElement].setAttribute('stroke', "#ff0000")
              poiSvgElements[svgElement].setAttribute('opacity', "1")
              poiSvgElements[svgElement].setAttribute('stroke-width', "2")
            }
          }
        }
      }, 10);
    }
  })
}

exports.poiSearchBackground = () => {
  let filterPoiName = ""
  let serverDown = false

  const listener = (details) => {
    let filter = browser.webRequest.filterResponseData(details.requestId)
    let decoder = new TextDecoder("utf-8")
    let encoder = new TextEncoder()

    let data = []
    filter.ondata = event => {
      data.push(decoder.decode(event.data, {stream: true}));
    }

    filter.onstop = event => {
      data.push(decoder.decode())
      let str = data.join("")
      json = JSON.parse(str)
      let search = []

      if (details.url.includes('nofilter')) {
        // The request come from modal list
        search = json.AllPOIs
      } else {
        // Filter response with search query
        search = json.AllPOIs.filter((poi)=> {
          return poi.name.includes(filterPoiName)
        })
      }

      let newPois = JSON.stringify({AllPOIs: search})


      filter.write(encoder.encode(newPois))
      filter.close();

      sendMessageToContentScript({ pois_fetched: true })
    }


    return {};
  }

  // browser.webRequest.onErrorOccurred.addListener( (responseDetails) => {
  //   console.log(responseDetails.url);
  //   console.log(responseDetails.error);
  //
  //   if (responseDetails.error == "NS_ERROR_CONNECTION_REFUSED") {
  //     serverDown = true
  //     sendMessageToContentScript({ server_down: true })
  //   }
  //
  // }, { urls: ["*://*/api/getwebuiupdates*"]  })

  browser.webRequest.onBeforeRequest.addListener( (details) => {
    // console.log(details);
    if (serverDown && details.responseSize > 0) {
      console.log(details);
      serverDown = false
      sendMessageToContentScript({ server_down: false })
    }
  }, {
    urls: [ "*://*/api/getwebuiupdates*", ]},
    ["blocking"]
  )

  browser.webRequest.onBeforeRequest.addListener( listener, {
    urls: [ "*://*/api/getallpois", ]},
    ["blocking"]
  )


  browser.runtime.onMessage.addListener((msg) => {
    if (typeof(msg.filterPoiName !== 'undefined')) {
      filterPoiName = msg.filterPoiName
    }
  });


}
