exports.activeCheckboxesStore = () => {
  browser.storage.local.get('checkboxes', (data) => {
    console.log("stored checkboxes:")
    console.log(data);

    if (Object.keys(data).length == 0) {
      browser.storage.local.set({"checkboxes": {0: false}}, function() {
        console.log('Checkboxes storage initialized')
      });
    }
  })

  let checkboxes = null
  const interval = setInterval( () => {
    checkboxes = document.getElementsByClassName("leaflet-control-layers-selector")
    if (checkboxes.length) {
      clearInterval(interval)

      browser.storage.local.get('checkboxes', function(data) {

        for (var entry in data.checkboxes) {
          if (data.checkboxes[entry]) {
            const checkbox = document.querySelector(`[data-box-index='${entry}']`);
            checkbox.click()
          }
        }

      })

      for (var check in checkboxes) {
        if (checkboxes.hasOwnProperty(check)) {
          checkboxes[check].dataset.boxIndex = check

          checkboxes[check].addEventListener("change", (e) => {
            console.log(check);
            browser.storage.local.get('checkboxes', function(item) {
              checkboxes = item.checkboxes
              checkboxes[e.target.dataset.boxIndex] = e.target.checked

              browser.storage.local.set({"checkboxes": checkboxes}, function() {
                console.log('Checkboxes saved');
              });
            })
          })
        }
      }
    }
  }, 10);



}
