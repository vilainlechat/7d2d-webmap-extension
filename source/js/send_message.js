exports.sendMessageToContentScript = (data) => {
  browser.tabs.query({
    currentWindow: true,
    active: true
  }).then((tabs) => {
    let tab = tabs[0]
    browser.tabs.sendMessage(
      tab.id,
      data
    )
  })
}

exports.sendMessageToBackground = (data) => {
  browser.runtime.sendMessage(data)
}
